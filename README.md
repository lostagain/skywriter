Basic concept for an open multi-source AR system using the WaveFederationProtocol.

## Goal; ## 

1. To allow anyone to write messages in midair, storing the real world location, as well as the message content, as a Blip within a Wave

2. To allow users to connect to and open one or more specific waves using their device. To allow users to see these messages from any and all Waves they have open.

## Requirements: ##

The protocol/framework developed should be able to be able to handle both loosely bounded facing text (todays "AR"), and in future tightly bound meshs skinning real world locations. (The AR goal) To achieve this protocol will be designed to enable posting 3d objects rather then just text, moving objects already posted to new locations, and allowing complex games to be developed based around Wave bots moving/changing the objects posted. Even if clients and hardware cant yet tie 3d data precisely to locations, the ability in the protocol should be there.

# Current State: #

You can make AR Billboards and post them to friends using XMPP. You can create multiple XMPP rooms, and select who you want to share them with.

Each room corresponds too a layer on the world, with any member in the room able too add to it.